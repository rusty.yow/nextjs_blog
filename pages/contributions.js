import siteMetadata from '@/data/siteMetadata'
import projectsData from '@/data/projectsData'
import Card from '@/components/Card'
import { PageSEO } from '@/components/SEO'

export default function contributions() {
  return (
    <>
      <PageSEO title={`Contributions- ${siteMetadata.author}`} description={siteMetadata.description} />
      <div className="divide-y divide-gray-200 dark:divide-gray-700">
        
      <div className="pt-10 pb-3 ">
        <h2 className="text-2xl font-bold tracking-tight leading-8">Contributions</h2>
        <p>Some Icons on this site have been provided by: <a href="https://icons8.com" class="underline decoration-dotted text-blue-600">Icons8.com</a></p>
      </div>

        
      </div>
    </>
  )
}
