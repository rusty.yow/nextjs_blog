import siteMetadata from '@/data/siteMetadata'
import projectsData from '@/data/projectsData'
import Card from '@/components/Card'
import { PageSEO } from '@/components/SEO'

export default function NewWorld() {
  return (
    <>
      <PageSEO title={`New World - ${siteMetadata.author}`} description={siteMetadata.description} />
      <div className="divide-y divide-gray-200 dark:divide-gray-700">
        
        <div className="container py-12">
          <div className="flex flex-wrap -m-4">
            <p>
          <a href="https://rustyyow.com/projects">Projects</a><br></br>
          <a href="https://rustyyow.com">Home</a>
          </p>
          </div>
        </div>
      </div>
    </>
  )
}
