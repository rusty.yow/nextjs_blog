import siteMetadata from '@/data/siteMetadata'
import projectsData from '@/data/projectsData'
import ListLayout from '@/layouts/ListLayout'
//import Tag from '@/components/Tag'
import Link from '@/components/Link'
//import Card from '@/components/Card'
import kebabCase from '@/lib/utils/kebabCase'
import { getAllTags } from '@/lib/tags'
import { PageSEO } from '@/components/SEO'
import { getAllFilesFrontMatter , getFileBySlug} from '@/lib/mdx'
import formatDate from '@/lib/utils/formatDate'

export async function getStaticProps( ) {
  const authorDetails = await getFileBySlug('authors', ['default'])
  const posts = await getAllFilesFrontMatter('blog') // Change string here to pick which folder.  Ex '', 'blog'
  const tags = await getAllTags('blog')

 


  return { props: { 'posts': posts, tags} }
}

export default function Projects({posts, tags}) {
//console.log("POSTS ARE:")
//console.log(posts)
 //console.log(tags)
 //console.log("DATE HERE:")
 //console.log(posts.date)
 
  
  return (
    <>
      
      <PageSEO title={`Projects - ${siteMetadata.author}`} description={siteMetadata.description} />

      <div className="divide-y ">
        
        <div className="pt-6 pb-8 space-y-2 md:space-y-5">

        <h1 className="text-3xl font-extrabold leading-9 tracking-tight text-gray-900 dark:text-gray-100 sm:text-4xl sm:leading-10 md:text-6xl md:leading-14">
            Projects
          </h1>
          <hr></hr>
          
          <ul>
            {
            
              posts.filter(p => p.tags.includes('Projects')).map(post => {
               // console.log(post)

                let date= post.date
                //console.log(date)

                let image = <></>
                if (post.images && post.images.length > 0) {
                  image = <img src={post.images[0]} width="200px" height="200px"/>
                }
                
                let tags = post.tags.map(tag => {
                  return (<Link className="mr-3 text-sm font-medium uppercase text-primary-500 hover:text-primary-600 dark:hover:text-primary-400" href={`/tags/${kebabCase(tag)}`}> {tag} </Link>)
                  })

                  

                return (
                  
                     
                      <li key={post.slug} className="py-4">
                        <article className="space-y-2 xl:grid xl:grid-cols-4 xl:space-y-0 xl:items-baseline">
                          
                      <dl>
                      <dt className="sr-only">Published on</dt>
                      <dd className="text-base font-medium leading-6 text-gray-500 dark:text-gray-400">
                      <time dateTime={date}>{formatDate(date)}</time>
                      {image}
                    </dd>
                  </dl>
                  <div className="space-y-3 xl:col-span-3">
                    <div>
                          <h3 className="text-2xl font-bold leading-8 tracking-tight">
                          <Link href={`/blog/${post.slug}`} className="text-gray-900 dark:text-gray-100">
                            {post.title}
                          </Link>
                        </h3>

                      <div className="flex flex-wrap">
                      {tags}
                      </div>

                    </div>
                    <div className="prose text-gray-500 max-w-none dark:text-gray-400">
                      {post.summary}
                    </div>
                  </div>
                </article>
              </li>
              
              
              





                    /* {/*   Title: <a href={`/blog/${post.slug}`}>{post.title}</a><br/>
                      Slug: {post.slug}<br/><br/>
                      
                      Tags: {tags}<br></br>
                      {image}
                      Summary: {post.summary}<br/><br/><br/><br/><br/> */
 

                    
                )
                         
                 
              })
            }
          
          </ul>

        </div>
        
      </div>
    </>
  )
}
