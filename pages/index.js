import Link from '@/components/Link'
import { PageSEO } from '@/components/SEO'
import Tag from '@/components/Tag'
import siteMetadata from '@/data/siteMetadata'
import { getAllFilesFrontMatter } from '@/lib/mdx'
import formatDate from '@/lib/utils/formatDate'
import { motion } from "framer-motion"
import { getFileBySlug } from '@/lib/mdx'
import { MDXLayoutRenderer } from '@/components/MDXComponents'


import NewsletterForm from '@/components/NewsletterForm'

const DEFAULT_LAYOUT = 'AuthorLayout'
const MAX_DISPLAY = 5

export async function getStaticProps() {
  const posts = await getAllFilesFrontMatter('blog')
  const authorDetails = await getFileBySlug('authors', ['default'])

  return { props: { posts, authorDetails } }
}

export default function About({ authorDetails }) {
  const { mdxSource, frontMatter } = authorDetails

  return (
    <>
      <PageSEO title={siteMetadata.title} description={siteMetadata.description} />
      <MDXLayoutRenderer
      layout={frontMatter.layout || DEFAULT_LAYOUT}
      mdxSource={mdxSource}
      frontMatter={frontMatter}
    />
      

      <div className="pt-10 pb-3 ">
        <h2 className="text-2xl font-bold tracking-tight leading-8">Skills</h2>
        <p>Some tools and technologies that I use on a regular basis.</p>
      </div>


      <div className="grid grid-cols-4 gap-4 text-center pt-0 mx-auto ">
        <div className="mx-auto">
            <motion.img 
            src="/static/images/skill_azure.png" 
            height="100" 
            width="100" 
            initial={{opacity: 0}}
            animate={{ opacity: 2 }}
            transition={{ delay: 0.5 }}
            whileHover={{
              scale: 1.5,
            }}
            >
              
            </motion.img>
        </div>

        <div className="mx-auto">
            <motion.img 
            src="/static/images/skills_vmware.png" 
            height="100" 
            width="100" 
            initial={{opacity: 0}}
            animate={{ opacity: 1 }}
            transition={{ delay: 0.7 }}
            whileHover={{
              scale: 1.5,
            }}
            >
              
            </motion.img>
        </div>


        <div className="mx-auto">
            <motion.img 
            src="/static/images/skills_veeam.png" 
            height="100" 
            width="100" 
            initial={{opacity: 0}}
            animate={{ opacity: 1 }}
            transition={{ delay: 0.9 }}
            whileHover={{
              scale: 1.5,
            }}
            >
              
            </motion.img>
        </div>
        

        <div className="mx-auto">
            <motion.img 
            src="/static/images/skills_aws.png" 
            height="100" 
            width="100" 
            initial={{opacity: 0}}
            animate={{ opacity: 1 }}
            transition={{ delay: 1 }}
            whileHover={{
              scale: 1.5,
            }}
            >
              
            </motion.img>
        </div>
        
        </div>



        <div className="pt-10 pb-3">
            <h2 className="text-2xl font-bold tracking-tight leading-8">Experience</h2>
        </div>


              <div className="space-y-5">
        <div className="p-3 dark:border-2 rounded-lg shadow">
          <p className="text-lg font-bold tracking-tight leading-8 dark:text-gray-400">UT Southwestern Medical Center</p>
          <p className="underline decoration-dotted text-blue-600">Enterprise System Administrator II</p>
          <p className="font-sans dark:text-gray-400">
          Responsible for the maintenance, configuration, and reliable operation of networked servers on prim and 
          cloud based. Provide support in a range of duties from client applications to cloud based processes to 
          the companies of UT Southwestern and Southwestern Health Resources.
       
          </p>
        </div>
        <div className="p-3 dark:border-2 rounded-lg shadow">
        <p className="text-lg font-bold tracking-tight leading-8 dark:text-gray-400">North Texas Specialty Physicians</p>
          <p className="underline decoration-dotted text-blue-600">Technical Support Analyst</p>
          <p className="font-sans dark:text-gray-400">
          North Texas Specialty Physicians, an independent physician association, exists to support the success of the more than 900 primary care and specialty physicians.
          During my time at NTSP I provided technical support to 4 Fort Worth locations and 1 North Carolina location.
          </p>
        </div>
      </div>




        <div className="pt-10 pb-3">
            <h2 className="text-2xl font-bold tracking-tight leading-8">Hobbies</h2>
            <p>My personal hobbies.</p>
        </div>

        <div className="grid grid-cols-4 gap-4 text-center pt-0 mx-auto ">
        <div className="mx-auto">
            <motion.img 
            src="/static/images/hobbies_gaming.png" 
            height="100" 
            width="100" 
            initial={{opacity: 0}}
            animate={{ opacity: 2 }}
            transition={{ delay: 0.5 }}
            whileHover={{
              scale: 1.5,
            }}
            >
              
            </motion.img>
        </div>

        <div className="mx-auto">
            <motion.img 
            src="/static/images/hobbies_hiking.png" 
            height="100" 
            width="100" 
            initial={{opacity: 0}}
            animate={{ opacity: 1 }}
            transition={{ delay: 0.7 }}
            whileHover={{
              scale: 1.5,
            }}
            >
              
            </motion.img>
        </div>


        <div className="mx-auto">
            <motion.img 
            src="/static/images/hobbies_dnd.png" 
            height="100" 
            width="100" 
            initial={{opacity: 0}}
            animate={{ opacity: 1 }}
            transition={{ delay: 0.9 }}
            whileHover={{
              scale: 1.5,
            }}
            >
              
            </motion.img>
        </div>
        

        <div className="mx-auto">
            <motion.img 
            src="/static/images/hobbies_tech.png" 
            height="100" 
            width="100" 
            initial={{opacity: 0}}
            animate={{ opacity: 1 }}
            transition={{ delay: 1 }}
            whileHover={{
              scale: 1.5,
            }}
            >
              
            </motion.img>
        </div>
        
        </div>
        
       
    
    </>

  )
}
