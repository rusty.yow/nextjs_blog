const headerNavLinks = [
  { href: 'https://rustyyow.com/tags/daily-blog', title: 'Blog' },
  { href: 'https://rustyyow.com/projects', title: 'Projects' },
  { href: 'https://rustyyow.com/tags', title: 'Tags' },
  { href: 'https://rustyyow.com', title: 'About' },
]

export default headerNavLinks
