const projectsData = [
  {
    title: 'Out with GatsbyJS, in With Next.js',
    description: `I moved on from GatsbyJS and am now using NextJS. I wanted to learn the basics of GatsbyJS and now 
    my site is using Next.js so I can play with that framework. Here is my very janky repository for my GatsbyJS site.`,
    imgSrc: '/static/images/mdx_code_example.png',
    href: 'https://gitlab.com/rusty.yow/rusty_blog',
  },
 
]

export default projectsData
